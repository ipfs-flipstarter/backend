const fs = require('fs');

module.exports = {
  command: 'add', 
  describe: 'add a flipstarter client', 
  builder: {
    cid: {
      alias: 'c',
      optional: true,
      demandOption: false,
    },
    file: {
      alias: 'f',
      optional: true,
      demandOption: false,
    }
  }, 
  async handler(argv, yargs) {
    //if daemon is running ask it to pull down the flipstarter and verify it, otherwise, we do so an update it, iA.
    //  flag for using ipfs-http-client vs ipfs itself, iA, and at what path, iA.
    if (!argv.ctx.isDaemon) {
      throw new Error("Daemon must be running for this operation");
    }

    /** @type {import('../../http-client/index.js')} */
    const backend = argv.ctx.backend;
    const { print } = argv.ctx;
    
    let result;

    if (argv.cid) {
      //TODO God willing: either fetch cid here or pass in to backend, iA.
    } else if (argv.file) {
      const campaign = await JSON.parse(await fs.promises.readFile(argv.file, "utf-8"));
      result = await backend.add(campaign);
    } else {
      throw new Error("Invalid arguments");
    }
    
    print(JSON.stringify(result, null, 2));
  }
}