const { createFlipstarterCampaignHtml, createFlipstarterCampaignSite } = require('@ipfs-flipstarter/core/');

const all = require('it-all');
const map = require('it-map');
const { pipe } = require('it-pipe');
const {toString:uint8ArrayToString} = require("uint8arrays/to-string")

/** @type {import('joi')} */
const Joi = require('../utils/joi');
const streamResponse = require('../utils/stream-response');
const { schema: campaignSchema } = require("../utils/campaign");

module.exports = {
  options: {
    validate: {
      failAction: (req, h, err) => {
        throw err;
      },
      options: {
        allowUnknown: true,
        stripUnknown: true
      },
      query: Joi.object({
        arg: Joi.cidAndPath().required()
      }),
    },
    cors: true
  },
  method: 'POST',
  path: '/api/v0/refs',
  handler: async function (request, h, response) {

    
    /** @type {import('ipfs').IPFS} */
    const ipfs = request.server.app.ipfs;
    
    /** @type {import('ipfs').CID} */
    const cid = request.query.arg && request.query.arg.cid;
    const invalidResult = { Ref: cid.toString(), Err: "Invalid" };
    
    /** @type {import('@ipld/dag-pb').PBNode} */
    const campaignDag = (await ipfs.dag.get(cid)).value;

    /** @type {Object<string,import('@ipld/dag-pb').PBLink} */
    const campaignLinks = campaignDag.Links.reduce((links, link) => {
      links[link.Name] = link;
      return links;
    }, {});

    const campaignDataLink = campaignLinks["campaign.json"];
    const campaignData = await JSON.parse(uint8ArrayToString(Buffer.concat(await all(ipfs.cat(campaignDataLink.Hash)))));
    const validationResult = campaignSchema.validate(campaignData);

    if (validationResult.error) {
      invalidResult.Err = validationResult.error;
      return invalidResult;
    }

    //TODO God willing: fetch verified dag based on campaign version
    const baseClientCid = request.server.app.baseClientCid;

    /** @type {import('@ipld/dag-pb').PBNode} */
    const baseClientDag = (await ipfs.dag.get(baseClientCid)).value;
    const baseDagLinks = baseClientDag.Links.reduce((links, link) => {
      links[link.Name] = link;
      return links;
    }, {});

    const staticLinksAreValid = verifyStaticLinks(campaignLinks, baseDagLinks);

    if (!staticLinksAreValid) {
      invalidResult.Err = "Invalid static links"
      return invalidResult;
    }

    //Verify the site's dynamic files (index.html) match what's expected
    const baseClientHtml = uint8ArrayToString(Buffer.concat(await all(ipfs.cat(baseDagLinks["index.html"].Hash))))
    const expectedIndexPageHtml = await createFlipstarterCampaignHtml(baseClientHtml, campaignData);
    const expectedIndexPageHash = (await ipfs.add(expectedIndexPageHtml, { onlyHash: true })).cid.toString();
    const actualIndexPageHash = campaignLinks["index.html"].Hash.toString();

    if (expectedIndexPageHash !== actualIndexPageHash) {
      invalidResult.Err = "index.html does not match expected hash"
      return invalidResult;
    }
     
    try {
  
      const hash = await createFlipstarterCampaignSite(ipfs, baseClientDag, expectedIndexPageHtml, campaignData);

      if (hash.toString() !== cid.toString()) {
        //This shouldn't be the case after our validation.
        console.log("Unexpected hash (actual, expected)", hash, cid);
      }

      return streamResponse(response, h, () => pipe(
        ipfs.refs(cid, {
          recursive: true 
        }), 
        async function * (source) {
          yield * map(source, ({ ref, err }) => ({ Ref: ref, Err: err || "" }))
        }
      ));

    } catch (err) {
      console.log(err);
      throw new Error("Failed to create campaign");
    }
  }
}

function verifyStaticLinks(actualDagLinks, expectedDagLinks) {
  //TODO God willing: new versions might have different comparitor functions
  const expectedFiles = ["campaign.json", "index.html", "static", "logo.ico"];
  const dynamicFiles = ["campaign.json", "index.html"];

  if (Object.keys(actualDagLinks).length !== expectedFiles.length) return false;
  
  for (let i=0; i < expectedFiles.length; i++) {
    const expectedFile = expectedFiles[i];
    const skip = dynamicFiles.indexOf(expectedFile) !== -1;

    if (skip) continue;

    const expectedStaticLink = expectedDagLinks[expectedFile];
    const actualCampaignLink = actualDagLinks[expectedFile];
    
    if (!actualCampaignLink || expectedStaticLink.Hash.toString() !== actualCampaignLink.Hash.toString()) {
      return false;
    }
  }

  return true;
}